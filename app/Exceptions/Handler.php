<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $e)
    {
        $e = $this->prepareException($e);
        $code = $e->getCode();
        $msg  = $e->getMessage();

        if (isset(request()->transaction) && $request->transaction) {
            DB::rollback();
        }
        if (!$code || $code > 599 || $code <= 0 || gettype($code) !== "integer") {
            $code = 500;
        }
        if($e instanceof NotFoundHttpException){
            $code = 404 ;
            $msg = 'Not found';
        }
        if($e instanceof AuthenticationException){
            $code = 403;
        }
        if ($e instanceof ValidationException) {
            $msg = $e->validator->errors()->all();
            return response()->json([
                'message' => 'invalid input.',
                'errors' => $msg
            ], 400);
        }
        if($e instanceof HttpResponseException){
            $code = $e->getResponse()->getStatusCode();
        }
        if ($e instanceof Responsable) {
            $code =  $e->toResponse($request)->getStatusCode();
        }
        return response()->json([
            'message' => $msg,
        ], $code);
    }
}
