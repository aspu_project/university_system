<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function index()
    {
        return $this->json(UserService::getAll());
    }

    public function authenticate(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users',
            'password' => 'required|string|min:6|max:255',
        ]);
        return $this->json(['token'=>UserService::authenticate($request->email,$request->password)]);
    }

}
