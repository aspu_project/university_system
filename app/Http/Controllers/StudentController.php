<?php

namespace App\Http\Controllers;

use App\Http\Resources\MessageResource;
use App\Http\Resources\StudentResource;
use App\Models\Student;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    public function index()
    {
        return $this->json(UserService::getAll());
    }

    public function getStudentWithMessages()
    {
        $student = Student::where('user_id',Auth::id())->firstOrFail();
        return $this->json(
            MessageResource::collection($student->Messages()->orderBy('created_at','DESC')->limit(10)->get())
        );
    }
}
