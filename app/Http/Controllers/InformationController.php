<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateInformationRequest;
use App\Http\Resources\InformationResource;
use App\Models\Information;
use App\Services\InformationService;
use Illuminate\Http\Request;

class InformationController extends Controller
{
    public function index(){
        return $this->json(InformationService::getAll());
    }

    public function store(CreateInformationRequest $request){
        return $this->json(InformationService::create($request->only(['title','description'])));
    }
    /**
     * Display the specified resource.
     */
    public function show(Information $information)
    {
        return $this->json(InformationResource::make($information));
    }


    public function update(Request $request, Information $information)
    {
        //
    }


    public function destroy(Information $information)
    {
        //
    }
}
