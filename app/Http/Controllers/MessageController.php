<?php

namespace App\Http\Controllers;

use App\Http\Resources\MessageResource;
use App\Models\Student;
use App\Services\ChatBotService;
use App\Services\MessageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    public function sendInfoMessage(Request $request){
        $request->validate([
            'question' => 'required|string|min:1|max:1023'
        ]);
        $student = Student::where('user_id',Auth::id())->firstOrFail();
        $answer = ChatBotService::sendInfoMessage($request->question);
        $message = MessageService::create([
            'student_id' => $student->id,
            'question' => $request->question,
            'answer' => $answer
        ]);
        return $this->json(
            MessageResource::make($message)
        );
    }
}
