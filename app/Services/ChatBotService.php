<?php
namespace App\Services;

use Exception;
use Illuminate\Support\Facades\Http;

class ChatBotService{

    // TODO :: edit url here
    private const baseUrl = 'https://a5f3-34-125-161-73.ngrok-free.app';

    static public function sendInfoMessage($message){
            $response = Http::post(self::baseUrl.'/public_answer',[
                'question'=>$message,
                'history' => MessageService::getLast10ForStudent()
            ]);
            if($answer = $response->json('answer'))
            return $answer;
        return "no bot answer right now.\npleas try again later or report the admin.";
    }

}

