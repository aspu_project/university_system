<?php
namespace App\Services;

use App\Http\Resources\StudentResource;
use App\Models\Student;
use Exception;
use Illuminate\Support\Facades\DB;

class StudentService {

    public static function getAll()
    {
        return StudentResource::collection(Student::all());
    }

    /**
     * @throws Exception
     */
    public static function create($data)
    {
        try {
            DB::beginTransaction();
            $user = UserService::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => $data['password'],
                'role' => 'student',
            ]);
            $student = Student::create([
               'user_id' => $user->id,
               'year' => $data['year'],
               'hours' => $data['hours'],
               'started_at' => $data['started_at'],
            ]);
            DB::commit();
            return $student;
        }catch (Exception $e){
            DB::rollBack();
            throw new Exception('fail to create user',500);
        }
    }

    public static function update($data,Student $student)
    {

    }

    public static function delete(Student $student)
    {

    }
}

