<?php
namespace App\Services;

use App\Http\Resources\InformationResource;
use App\Models\Information;

class InformationService {

    public static function getAll()
    {
        return InformationResource::collection(Information::query()->orderByDesc('created_at')->get());
    }

    public static function create($data)
    {
        $information = Information::create($data);
        // send
        return InformationResource::make($information);
    }

    public static function update($data,Information $information)
    {

    }

    public static function delete(Information $information)
    {

    }
}

