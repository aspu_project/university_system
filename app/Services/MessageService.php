<?php
namespace App\Services;

use App\Http\Resources\MessageResource;
use App\Models\Message;
use App\Models\Student;
use Illuminate\Support\Facades\Auth;

class MessageService {

    public static function getAll()
    {
        return MessageResource::collection(Message::all());
    }

    public static function create($data)
    {
        return Message::create($data);
    }

    public static function getLast10ForStudent()
    {
        $s = Student::where('user_id',Auth::id())->firstOrFail();
        $m = Message::where('student_id',$s->id)->limit(10)->get();
        $arr = [];
        foreach ($m as $message){
            array_push($arr,[$message->question,$message->answer,]);
        }
        return $arr;
    }

}

