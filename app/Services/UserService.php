<?php
namespace App\Services;

use App\Http\Resources\UserResource;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserService {

    public static function getAll()
    {
        return UserResource::collection(User::all());
    }

    public static function create($data)
    {
        $data['password'] = Hash::make($data['password']);
        return UserResource::make(User::create($data));
    }

    public static function authenticate($email,$password)
    {
        if(Auth::attempt(['email' => $email , 'password' => $password])){
            return User::where('email',$email)->firstOrFail()->createToken('auth')->plainTextToken;
        }
        throw new Exception('wrong password',401);
    }

    public static function update($data,User $user)
    {

    }

    public static function delete(User $user)
    {

    }
}

