<?php

namespace App\Traits;

trait JsonResponseTrait
{
    public function json($data = [],$status = 200,$message = ''){
        return response()->json(
            [
                'data' =>$data,
                'message' => $message
            ],
            $status
        );
    }
}
