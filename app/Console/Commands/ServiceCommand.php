<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Symfony\Component\Console\Input\InputOption;

class ServiceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:service {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new service ';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $name = $this->argument('name');
        $model = $this->option('model');
        if(isset($model)) {
            $stub = File::get(resource_path('stubs/ServiceModelStub.stub'));
            if(!File::exists(app_path("Models/$model.php"))){
                $this->error("model $model is not exist");
                return;
            }
        }
        else
            $stub = File::get(resource_path("stubs/ServiceStub.stub"));

        $serviceStub = str_replace('{{ServiceName}}', $name, $stub);
        $serviceStub = str_replace('{{model}}', $model, $serviceStub);
        $serviceStub = str_replace('{{modelLower}}', strtolower($model), $serviceStub);

        $servicePath = app_path("Services/$name.php");

        if (File::exists($servicePath)) {
            $this->error("$name already exists!");
        }
        else {
            File::put($servicePath, $serviceStub);
            $this->info("Service $name has been created.");
        }
    }

    protected function configure()
    {
        $this->addOption('model','model',InputOption::VALUE_OPTIONAL);
    }
}
