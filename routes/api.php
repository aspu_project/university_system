<?php

use App\Http\Controllers\InformationController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login',[UserController::class,'authenticate']);

Route::apiResource('/information',InformationController::class)
    ->only(['show','index']);

Route::middleware(['auth:sanctum','student'])->group(function(){
    Route::post('send_info_message',[MessageController::class,'sendInfoMessage']);
    Route::get('old_info_messages',[StudentController::class,'getStudentWithMessages']);
});


Route::middleware(['auth:sanctum','admin'])->group(function(){
    Route::apiResource('/information',InformationController::class)
        ->except(['show','index']);
});
