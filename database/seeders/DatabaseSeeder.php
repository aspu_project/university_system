<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Student;
use App\Services\StudentService;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */

    private const password = '123456';

    public function run(): void
    {
        StudentService::create([
            'name' => 'student a',
            'email' => 'student@aspu.com',
            'password' => self::password,
            'year' => 2,
            'hours' => 82,
            'started_at' => Carbon::create(2021, 8, 15),
        ]);

        UserService::create([
            'name' => 'admin',
            'email' => 'admin@aspu.com',
            'password' => self::password,
            'role' => 'admin'
        ]);
    }
}
